import json
from pathlib import Path
from typing import TypeVar
from uuid import uuid4

from .repo import CKANRepo
from .utils import detect_version

path_t = TypeVar("pathT", str, Path)


class Instance:
    def __init__(self, root_path: str, name="ANON"):  # Sometimes we don't really need a named Instance.
        self.root_path = Path(root_path)
        self.name = name
        self.version = detect_version(Path(root_path))
        if not self._get_path().exists():
            Instance.create(root_path)
        obj = json.load(self._get_path().open())
        self.repos = {uuid: CKANRepo(self, uuid, **details) for uuid, details in obj['repos'].items()}
        self.packages = obj["packages"]

    def get_repo_path(self, uuid) -> Path:
        path = self.root_path / "CKAN.py/repos" / uuid
        if not path.exists():
            path.mkdir(parents=True)
        return path

    def _get_path(self) -> Path:
        return self.root_path / "CKAN.py/settings.json"

    @staticmethod
    def create(kspdir: str):
        obj = {
            "repos": {
                str(uuid4()): {"name": "default",
                               "uri": "https://github.com/KSP-CKAN/CKAN-meta/archive/master.tar.gz"
                               }
            },
            "packages": []
        }
        if not (Path(kspdir) / "CKAN.py").exists():
            (Path(kspdir) / "CKAN.py").mkdir(parents=True)
        with (Path(kspdir) / "CKAN.py/settings.json").open("w") as f:
            f.write(json.dumps(obj, indent=4))
        return Instance(kspdir)

    def check_package_installed(self, identifier):
        if [pkg for pkg in self.packages if pkg["identifier"] == identifier]:
            return True
        else:
            return False

    def add_package_record(self, identifier, version, files):
        self.packages.append({"identifier": identifier, "version": version, "files": files})
        self.save()

    def remove_package_record(self, identifier):
        self.packages = [pkg for pkg in self.packages if pkg['identifier'] != identifier]
        self.save()

    def __repr__(self):
        return "Instance('{}',{})".format(str(self.root_path), self.version)

    def save(self):
        obj = {
            "repos": {uuid: {"name": repo.name, "uri": repo.uri} for uuid, repo in self.repos.items()},
            "packages": self.packages
        }
        with self._get_path().open("w") as f:
            f.write(json.dumps(obj, indent=4))


class Settings:
    def __init__(self):
        obj = json.load((Path("~") / ".ckanpy.cfg").expanduser().open())
        curr_uuid = obj["current_instance"]
        self.instances = {uuid: Instance(details["path"], details["name"]) for uuid, details in
                          obj['instances'].items()}
        self.current_instance = self.instances[curr_uuid]

    @staticmethod
    def create(kspdir: str):
        default_uuid = str(uuid4())
        obj = {
            "instances": {default_uuid: {"name": "Default", "path": kspdir}},
            "current_instance": default_uuid
        }
        with (Path("~") / ".ckanpy.cfg").expanduser().open("w") as f:
            f.write(json.dumps(obj, indent=4))
        return Settings()

    def get_instance_by_name(self, name):
        try:
            return Instance(next(instance for uuid, instance in self.instances.items() if instance["name"] == name)["path"], name)
        except StopIteration:
            raise Exception("Instance lookup failed.")

    def save(self):
        obj = {
            "instances": {
                uuid: {"name": inst.name, "path": inst.path} for uuid, inst in self.instances.items()}
        }
