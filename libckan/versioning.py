import re


def _str_comp(a: str, b: str):
    """
    Strictly follows https://github.com/KSP-CKAN/CKAN/blob/master/Core/Types/Version.cs#L76
    It's a delicate process.
    """
    ar = ""
    br = ""
    for idx, char in enumerate(a):
        if char.isdigit():
            ar = a[idx:]
            a = a[:idx]
            break
    for idx, char in enumerate(b):
        if char.isdigit():
            br = b[idx:]
            b = b[:idx]
            break
    ret = 0
    if len(a) > 0 and len(b) > 0:
        if a[0] != "." and b[0] == ".":
            ret = -1
        elif a[0] == "." and b[0] != ".":
            ret = 1
        elif a[0] == "." and b[0] == ".":
            if len(a) == 1 and len(b) > 1:
                ret = 1
            elif len(a) > 1 and len(b) == 1:
                ret = -1
        else:
            if a < b:
                ret = -1
            elif a == b:
                ret = 0
            elif a > b:
                ret = 1
    else:
        if a < b:
            ret = -1
        elif a == b:
            ret = 0
        elif a > b:
            ret = 1
    return ret, ar, br


def _num_comp(a, b):
    """This too."""
    ar = ""
    br = ""
    for idx, char in enumerate(a):
        if not char.isdigit():
            ar = a[idx:]
            a = a[:idx]
            break
    for idx, char in enumerate(b):
        if not char.isdigit():
            br = b[idx:]
            b = b[:idx]
            break
    ret = 0
    ai = 0 if len(a) == 0 else int(a)
    bi = 0 if len(b) == 0 else int(b)
    if ai < bi:
        ret = -1
    elif ai == bi:
        ret = 0
    elif ai > bi:
        ret = 1
    return ret, ar, br


def version_comp(a, b):
    while len(a) != 0 and len(b) != 0:
        ret, a, b = _str_comp(a, b)
        if ret != 0:
            return ret
        ret, a, b = _num_comp(a, b)
        if ret != 0:
            return ret
    if len(a) == 0 and len(b) == 0:
        return 0
    else:
        return -1 if len(a) == 0 else 1


class CKANVersion:
    def __init__(self, version_string):
        self.version_string = version_string
        match = re.match(r"((\d+):)?(.+)", version_string)
        if not match.group(2):
            self.epoch = 0
        else:
            self.epoch = int(match.group(2))
        self.version = match.group(3)

    def __lt__(self, other):
        if self.epoch < other.epoch:
            return True
        elif self.epoch == other.epoch and version_comp(self.version, other.version) == -1:
            return True
        else:
            return False

    def __gt__(self, other):
        if self.epoch > other.epoch:
            return True
        elif self.epoch == other.epoch and version_comp(self.version, other.version) == 1:
            return True
        else:
            return False

    def __eq__(self, other):
        if self.epoch == other.epoch and version_comp(self.version, other.version) == 0:
            return True
        return False

    def __ge__(self, other):
        return self == other or self > other

    def __le__(self, other):
        return self == other or self < other

    def __str__(self):
        return self.version_string

    def __repr__(self):
        return self.version_string


class CKANAtom:
    def __init__(self, *kargs):
        if len(kargs) == 1:  # constructing from a atom string
            m = re.fullmatch(r"((?:>=)|(?:<=)|(?:=)|(?:>)|(?:<))?(.*?)(?:-(.*))?", kargs[0])
            self.op = m.group(1) or None
            self.identifier = m.group(2)
            self.version = CKANVersion(m.group(3)) if m.group(3) else None
        elif len(kargs) == 3:  # constructing from the parts
            if kargs[0] not in [">", "<", "=", ">=", "<="]:
                raise Exception("Illegal operator")
            self.op = kargs[0]
            self.identifier = kargs[1]
            self.version = CKANVersion(kargs[2])

    def __contains__(self, item):
        if self.match(item):
            return True
        return False

    def match(self, version):
        if isinstance(version, str):
            version = CKANVersion(version)
        if self.op == "=":
            return version == self.version
        elif self.op == ">":
            return version > self.version
        elif self.op == ">=":
            return version >= self.version
        elif self.op == "<":
            return version < self.version
        elif self.op == "<=":
            return version <= self.version
        elif not self.op:
            return True

    def find_build(self, repo):
        results = repo.search([lambda x: x["identifier"] == self.identifier])
        if len(results) > 1:
            raise Exception("Too many matches")
        elif len(results) == 0:
            raise Exception("No match")
        return results[0].get_builds(lambda b: b["version"] in self)

    def __repr__(self):
        return "('{}','{}','{}')".format(self.op, self.identifier, self.version)
