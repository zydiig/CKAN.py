import re
from hashlib import sha1, sha256
from pathlib import Path
from urllib.parse import urlparse


def get_filename_from_url(url):
    return Path(urlparse(url)[2]).name


def get_sha1(data):
    h = sha1()
    h.update(data)
    return h.hexdigest().upper()


def get_sha256(data):
    h = sha256()
    h.update(data)
    return h.hexdigest().upper()


def detect_version(root_path: Path):
    with (root_path / "readme.txt").open(encoding="cp1252") as f:  # What encoding does readme.txt use anyway?
        for line in f:
            match = re.fullmatch("Version ([0-9\.]+)", line.strip())
            if match:
                return match.group(1)
    raise Exception("Could not recognize the version of KSP instance.")


def ask_for_permission(message):
    s = input(message)
    if s.lower() in ["y", "yes", "yep", "yeah"]:
        return True
    else:
        return False
