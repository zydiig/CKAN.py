import logging
import shutil
import subprocess
import zipfile
from pathlib import Path
from pprint import pformat

from .config import Instance
from .fs import find_by_regexp, find_by_name, is_relative_to, gen_file_list
from .structures import Filter
from .utils import get_filename_from_url, get_sha1, get_sha256, ask_for_permission


def check_digests(fpath, digests):
    data = fpath.open("rb").read()
    ret_sha1 = True
    ret_sha256 = True
    if "sha1" in digests:
        ret_sha1 = get_sha1(data) == digests["sha1"]
    if "sha256" in digests:
        ret_sha256 = get_sha256(data) == digests["sha256"]
    return ret_sha1 and ret_sha256


def download_to(url, dir, name, digests=None):
    dir = str(dir) if type(dir) is Path else dir
    fpath = Path(dir) / name
    print(fpath)
    if fpath.exists() and digests:
        if check_digests(fpath, digests):
            return fpath
    try:
        subprocess.check_call(
            ["aria2c", url, "-d", dir, "-o", name, "--allow-overwrite", "--summary-interval=0"])  # TODO: Support more downloaders
    except subprocess.CalledProcessError:
        logging.error("Error downloading {}".format(url))
        return None
    if digests:
        if not check_digests(fpath, digests):
            raise Exception("Digest verification failed.")
    return fpath


class SafetyException(Exception):
    pass


def extract(path: Path, dest):  # Will there be files other than .zip's?
    logging.info(("Extracting to ", dest))
    if not path.exists():
        path.mkdir(parents=True)
    f = zipfile.ZipFile(str(path))
    f.extractall(path=dest)


def remove(identifier, instance, dry_run=False):
    for pkg in instance.packages:
        if pkg['identifier'] == identifier:
            dirs = []
            for entry in pkg['files']:
                p_entry = instance.root_path / entry
                if not p_entry.exists():
                    continue
                if p_entry.is_file():
                    if not dry_run:
                        p_entry.unlink()  # It's just fancy rm.
                    print("RM {}".format(str(p_entry)))
                elif p_entry.is_dir():
                    dirs.append(p_entry)
            for p_dir in reversed(dirs):
                try:
                    if not dry_run:
                        p_dir.rmdir()
                    print("RMDIR {}".format(str(p_dir)))
                except:
                    print("Could not remove {}".format(str(p_dir)))
    if not dry_run:
        instance.remove_package_record(identifier)


def install(path, directives, instance, identifier, dry_run=False, overwrite=False):
    tmpdir = Path("/tmp/CKAN.py/") / path.stem  # FIXME: Unix only
    extract(path, str(tmpdir))
    filters = []
    if not directives:
        directives = [{"find": identifier, "install_to": "GameData"}]
    logging.debug(directives)
    for directive in directives:
        if "file" in directive:
            src = tmpdir / directive["file"]
        elif "find" in directive:  # Spec v1.4
            src = tmpdir / find_by_name(tmpdir, directive["find"])
        elif "find_regexp" in directive:  # Spec v1.10
            src = tmpdir / find_by_regexp(tmpdir, directive["find_regexp"])
        else:
            raise Exception("Unrecognized directive type(source).")
        if "optional" in directive and directive["optional"]:
            # opt = input('The install item "{}" is optional.Would you like to install it? (Y/n) '.format(src))
            opt = ask_for_permission(
                'The install item "{}" is optional.Would you like to install it? (Y/n) '.format(src))
            if not opt:
                print("Declined: {}".format(src))
                continue
            else:
                print("Accepted: {}".format(src))
        dest = instance.root_path if directive["install_to"] == "GameRoot" else instance.root_path / directive[
            "install_to"]
        dest /= src.name
        if not is_relative_to(dest, instance.root_path):
            raise SafetyException("Attempting to write to {}".format(dest))
        if not (directive["install_to"] in ["GameData", "Ships", "Ships/SPH", "Ships/VAB", "Ships/@thumbs/VAB", "Ships/@thumbs/SPH", "Tutorial",
                                            "Scenarios", "GameRoot"] or directive["install_to"].startswith("GameData/")):
            raise SafetyException("Illegal install destination.")
        if "as" in directive:
            dest = dest.with_name(directive["as"])
        if "filter" in directive:
            if type(directive["filter"]) is list:
                filters += [Filter(Filter.NAME, filter_string) for filter_string in directive["filter"]]
            else:
                filters.append(Filter(Filter.NAME, directive["filter"]))
        if "filter_regexp" in directive:
            if type(directive["filter_regexp"]) is list:
                filters += [Filter(Filter.REGEXP, filter_string) for filter_string in directive["filter_regexp"]]
            else:
                filters.append(Filter(Filter.REGEXP, directive["filter_regexp"]))
        # TODO : find_matches_files (Spec v1.16)
        records = []
        for file in gen_file_list(src, filters=filters):
            if (dest / file).is_file() and (dest / file).exists() and not overwrite:
                raise Exception("File collision: {}".format(str(dest / file)))
            if (src / file).is_dir() and not (dest / file).exists():
                if not dry_run:
                    (dest / file).mkdir(parents=True)
                    records.append(str((dest / file).relative_to(instance.root_path)))
                print("DIR ", dest / file)
            elif (src / file).is_file():
                if not dry_run:
                    shutil.copy(str(src / file), str(dest / file))
                    records.append(str((dest / file).relative_to(instance.root_path)))
                print("FILE {src} -> {dest}".format(src=src / file, dest=dest / file))
        return records


def install_package(instance: Instance, build, dry_run, overwrite=False, no_digest=False):
    logging.debug(pformat(build))
    if instance.check_package_installed(build["identifier"]):
        logging.error("{} is already installed.".format(build['identifier']))
        return
    path = download_to(build["download"], "/tmp/CKAN.py/fetch", get_filename_from_url(build["download"]),
                       build["download_hash"] if not no_digest else None)  # TODO: Portable tmpfiles.
    files = install(path, build.get("install", None), instance, build["identifier"], dry_run, overwrite=overwrite)
    if not dry_run:
        instance.add_package_record(build["identifier"], build['version'], files)


def resolve_deps(instance, atoms):
    pass
