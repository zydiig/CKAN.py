import json
import logging
import tarfile

import requests
from packaging import version

from libckan.versioning import CKANVersion, CKANAtom


class CKANPackage:
    def __init__(self, identifier, name, builds, instance=None):
        self.identifier = identifier
        self.name = name
        self.builds = builds
        self.instance = instance

    def get_all_versions(self):
        return sorted([CKANVersion(build["version"]) for build in self.builds], reverse=True)

    def __getattr__(self, item):
        if item in self.get_lastest_build():
            return self.get_lastest_build()[item]

    def __repr__(self):
        return str((self.identifier, self.name))

    def get_lastest_build(self, conds=None, compatible=True):
        builds = self.get_builds(conds, compatible)
        if builds:
            return builds[0]  # self.get_builds() is already sorted.

    def get_builds(self, conds=None, compatible=True):
        conds = conds or []
        if compatible and self.instance:
            builds = self._filter_by_ksp_version(self.instance.version)
        else:
            builds = self.builds

        def new_filter(x):
            return all([cond(x) for cond in conds])

        return sorted(list(filter(new_filter, builds)), key=lambda x: CKANVersion(x["version"]), reverse=True)

    def _filter_by_ksp_version(self, ksp_version):
        ret = []
        for build in self.builds:
            if (("ksp_version" not in build) and ("ksp_version_min" not in build) and (
                        "ksp_version_max" not in build)) or ("ksp_version" in build and build["ksp_version"] == "any"):
                ret.append(build)
            elif "ksp_version_min" in build or "ksp_version_max" in build:
                if "ksp_version_min" in build and not version.parse(build["ksp_version_min"]) <= version.parse(
                        ksp_version):
                    continue
                if "ksp_version_max" in build and not version.parse(build["ksp_version_max"]) >= version.parse(
                        ksp_version):
                    continue
                ret.append(build)
            else:
                if build["ksp_version"].count(".") == 1 and ksp_version.startswith(build["ksp_version"]):
                    ret.append(build)
                    # TODO: Implement ksp_version_strict
        return ret


class CKANRepo:
    def __init__(self, instance, uuid, name, uri, rebuild=False):
        self.instance = instance
        self.uuid = uuid
        self.name = name
        self.uri = uri
        self.path = self.instance.get_repo_path(uuid) / "cache.json"
        if not self.path.exists() or rebuild:
            self.parse_metadata()
        else:
            with self.path.open("r") as f:
                self.packages = json.load(f)

    def parse_metadata(self):
        tarball_path = self.instance.get_repo_path(self.uuid) / self.uuid
        if not tarball_path.exists():
            with (self.instance.get_repo_path(self.uuid) / self.uuid).open("wb") as f:
                f.write(requests.get(self.uri).content)
        tf = tarfile.open(str(tarball_path))
        packages = []
        for member in tf.getmembers():
            if member.isreg() and member.name.endswith(".ckan"):
                package = json.loads(tf.extractfile(member).read().decode("utf-8"))
                packages.append(package)
        logging.info("Successfully parsed {} ckan files.".format(str(len(packages))))
        self.packages = packages
        if not self.path.parent.exists():
            self.path.parent.mkdir(parents=True)
        with self.path.open("w") as f:
            f.write(json.dumps(self.packages, indent=4))

    def search(self, conds):

        def cond_new(x):
            return all([cond(x) for cond in conds])

        filtered = list(filter(cond_new, self.packages))
        merged = {pid: [] for pid in [package["identifier"] for package in filtered]}
        for package in filtered:
            merged[package["identifier"]].append(package)  # Merge multiple versions of .ckan file into one entry.
        packages = []
        for identifier, builds in merged.items():
            packages.append(
                CKANPackage(identifier, max(builds, key=lambda build: CKANVersion(build["version"]))["name"],
                            builds, self.instance))  # Use the name of the newest CKAN package as the 'canonical' name to shown
        return packages

    def resolve_atom(self, atom: CKANAtom, compatible=True):
        results = self.search([lambda x: x["identifier"] == atom.identifier])
        if results:
            return results[0].get_builds([lambda b: b['version'] in atom], compatible=compatible)
        else:
            return []
