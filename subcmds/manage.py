import logging

from libckan import install as _install
from libckan.utils import ask_for_permission
from libckan.versioning import CKANAtom, CKANVersion


# Package management

def install(args):
    instance = args.instance
    results = []
    for repo in instance.repos.values():
        for atom in args.atom:
            logging.debug(CKANAtom(atom))
            results.append(max(repo.resolve_atom(CKANAtom(atom)), key=lambda x: CKANVersion(x["version"])))
    # if len(results) > 1:
    #     for idx, pkg in enumerate(results):
    #         print("[{}]".format(idx), pkg.name)
    #     try:
    #         indices = [int(ns) for ns in input("Which to install (Space seperated):").split(" ")]
    #     except ValueError:
    #         print("Incorrect input.")
    #         return
    #     results = [result for idx, result in enumerate(results) if idx in indices]
    if len(results) == 0:
        print("ERROR: No such package.")
    for pkg in results:
        _install.install_package(instance, pkg, dry_run=args.dry_run, overwrite=args.overwrite, no_digest=args.no_digest)


def remove(args):
    instance = args.instance
    for identifier in args.id:
        if instance.check_package_installed(identifier):
            print("REMOVING :", identifier)
            _install.remove(identifier, instance, dry_run=True)
            if ask_for_permission("Remove? (y/n) "):
                _install.remove(identifier, instance)
            else:
                print("User declined.")
        else:
            print("Package {} not found.".format(identifier))
            break
