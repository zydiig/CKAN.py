import re
from pprint import pprint

from pkg_resources import parse_version

from libckan.versioning import CKANAtom, CKANVersion


def entry(args):
    conds = [lambda _: True]
    if args.name:
        conds.append(lambda x: bool(re.match(args.name, x["name"])))
    if args.desc:
        conds.append(lambda x: bool(re.match(args.desc, x["abstract"])))
    if args.kspver:
        conds.append(lambda x: parse_version(args.kspver) == parse_version(x["ksp_version"]))
    if args.keyword:
        conds.append(lambda x: bool(re.match(args.keyword, x["name"])) or bool(re.match(args.keyword, x["abstract"])))
    for repo in args.instance.repos.values():
        for package in repo.search(conds):
            versions = sorted(
                [CKANVersion(build["version"]) for build in package.get_builds(compatible=not args.show_all)], reverse=True)
            if len(versions) == 0:
                continue
            print("")
            print("Identifier:  ", package.id)
            print("Name:        ", package.name)
            print("Author:      ", package.author or "")
            print("Description: ", package.abstract)
            print("License:     ", package.license)
            print("Versions:    ", versions)
            print("Resources:   ")
            pprint(package.resources or "")
            print("")


def resolve(args):
    atom = CKANAtom(args.atom)
    results = []
    for repo in args.instance.repos.values():
        results.append(repo.resolve_atom(atom, compatible=not args.show_all))
    pprint(results)
