CKAN.py
========
A **WIP** Python 3 client for the Comprehensive Kerbal Archive Network (CKAN).

May wreck your KSP installations.*Use at your own risk.*


## To do
- Dependency resolution
- Better search
- Improve user interaction
- Cleanup
- Strip optional dependencies from install()